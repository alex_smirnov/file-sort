package task;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import org.junit.Test;

import static org.junit.Assert.*;

public class LargeFileSorterTest {

    @Test
    public void sort() throws IOException, ExecutionException, InterruptedException {

        Path source = Paths.get("./file.txt");
        Path target = Paths.get("./sorted.txt");

        RandomFileGenerator generator = new RandomFileGenerator();
        generator.saveNewFile(3_000_000, 500, source);

        System.out.println("Random file generated. Start file sorting...");

        LargeFileSorter sorter = new LargeFileSorter(source, target);
        sorter.sort();

        System.out.println("File sorted. Starting checks...");

        // Файл не влезает в память, но мы можем посчитать количество строк по хэшкодам,
        // чтобы убедиться, что все строки из оригинального файла присутствуют в отсортированном

        // Это не является строгим доказательством корректности, но на практике позволит выявить проблему в сортировке
        Map<Integer, Integer> linesCountByHashCode = new HashMap<>();

        try (BufferedReader reader = Files.newBufferedReader(source)) {
            Iterator<String> iterator = reader.lines().iterator();
            while (iterator.hasNext()) {
                String line = iterator.next();
                int hashCode = line.hashCode();
                Integer count = linesCountByHashCode.get(hashCode);
                linesCountByHashCode.put(hashCode, count == null ? 1 : count + 1);
            }
        }

        try (BufferedReader reader = Files.newBufferedReader(target)) {

            Iterator<String> iterator = reader.lines().iterator();
            String previousLine = null;

            while (iterator.hasNext()) {
                String line = iterator.next();
                if (previousLine != null) {
                    assertTrue("Wrong sorting order", line.compareTo(previousLine) >= 0);
                }

                int hashCode = line.hashCode();
                Integer countWithThatHashcode = linesCountByHashCode.get(hashCode);
                assertNotNull("No string in original file with hash code " + hashCode, countWithThatHashcode);
                assertTrue("More strings in sorted file with hash code " + hashCode, --countWithThatHashcode >= 0);
                linesCountByHashCode.put(hashCode, countWithThatHashcode);

                previousLine = line;
            }
        }

        for (Map.Entry<Integer, Integer> entry : linesCountByHashCode.entrySet()) {
            assertEquals("String is missing in sorted file " + entry.getKey(), 0, (int) entry.getValue());
        }

        System.out.println("All checks are ok!");

        // не будем удалять файлы, чтобы можно было вручную посмотреть на результат

//        Files.delete(source);
//        Files.delete(target);

    }
}