package task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;

import org.junit.Test;

import static org.junit.Assert.*;

public class RandomFileGeneratorTest {

    @Test
    public void saveNewFile() throws IOException {

        Path filePath = Paths.get("./random-file.txt");

        RandomFileGenerator generator = new RandomFileGenerator();
        int expectedLinesCount = 100_000;
        int expectedMaxLineLength = 200;
        generator.saveNewFile(expectedLinesCount, expectedMaxLineLength, filePath);

        int linesCount = 0;
        int maxLineLength = -1;

        try (BufferedReader reader = Files.newBufferedReader(filePath)) {
            Iterator<String> iterator = reader.lines().iterator();
            while (iterator.hasNext()) {
                String line = iterator.next();
                linesCount++;
                if (line.length() > maxLineLength) {
                    maxLineLength = line.length();
                }
            }
        }

        assertEquals(expectedLinesCount, linesCount);
        assertEquals(expectedMaxLineLength, maxLineLength);

        Files.delete(filePath);
    }
}