package task;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Генератор строковых файлов.
 * Позволяет получить файл с заданным количеством строк случайной длины (в пределах переданных параметров).
 */
public class RandomFileGenerator {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    public void saveNewFile(int linesCount, int maxLineLength, Path filePath) throws IOException {
        Random random = ThreadLocalRandom.current();
        try (BufferedWriter writer = Files.newBufferedWriter(filePath, CHARSET)) {
            while (--linesCount >= 0) {
                writer.write(generateRandomString(random, maxLineLength));
                writer.newLine();
            }
        }
    }

    private String generateRandomString(Random random, int maxLineLength) {
        int length = random.nextInt(maxLineLength) + 1;
        StringBuilder builder = new StringBuilder(length);
        while (--length >= 0) {
            builder.append(getRandomChar(random, '!', '~'));
        }
        return builder.toString();
    }

    private char getRandomChar(Random random, char start, char end) {
        return (char) (random.nextInt(end - start) + start);
    }

    public static void main(String[] args) throws IOException {
        new RandomFileGenerator().saveNewFile(3_000_000, 500, Paths.get("./file.txt"));
    }

}
