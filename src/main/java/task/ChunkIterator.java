package task;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * Итератор по файлу со строками, позволяющий запоминать текущую строку.
 */
public class ChunkIterator {

    private Path path;
    private BufferedReader reader;
    private Iterator<String> iterator;
    private String current;

    public ChunkIterator(Path path, BufferedReader reader) {
        this.path = path;
        this.reader = reader;
        this.iterator = reader.lines().iterator();
    }

    public boolean hasNext() {
        return iterator.hasNext();
    }

    public String next() {
        current = iterator.next();
        return current;
    }

    public String getCurrent() {
        return current;
    }

    public void closeAndDelete() throws IOException {
        reader.close();
        Files.delete(path);
    }
}
