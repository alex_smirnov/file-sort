package task;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Сортирует большой файл, который может не помещаться в память целиком, разбивая файл на куски и сортируя каждый по отдельности.
 *
 * Размер портребляемой памяти зависит от:
 * - chunkSize - размера одного чанка, сортируемого в памяти
 * - threadsCount - количества потоков
 *
 * Для каждой сортировки нужно создать новый инстанс этого класса и вызывать метод {@link LargeFileSorter#sort()}
 *
 * Есть несколько возможностий для дальнейшего улучшения этого класса:
 * - если нужно отсортировать много файлов, можно модифицировать его так, чтобы переиспользовать однажны созданный инстанс вместе с пулом потоков
 * - попробовать сделать автоматическое определение оптимального количества потоков и размера чанка
 * - применить специфичный именно для строк алгоритм сортировки (учитывая, что в строке ограниченное заранее известное множество символов)
 * - попробовать более эффективно мержить файлы на финальном этапе
 */
public class LargeFileSorter {

    private static final Charset CHARSET = StandardCharsets.UTF_8;

    private final Path sourceFilePath;
    private final Path targetFilePath;
    private Path tempPath;
    private final Comparator<String> comparator;
    private final int chunkSize;
    private final ExecutorService executorService;

    public LargeFileSorter(Path sourceFilePath, Path targetFilePath, Comparator<String> comparator, int chunkSize, int threadsCount) {
        this.sourceFilePath = sourceFilePath;
        if (!Files.isRegularFile(this.sourceFilePath)) {
            throw new IllegalStateException("Source file not found: " + sourceFilePath);
        }
        this.targetFilePath = targetFilePath;

        if (comparator == null) {
            comparator = Comparator.naturalOrder();
        }
        this.comparator = comparator;

        if (chunkSize <= 0) {
            throw new IllegalArgumentException("chunkSize must be > 0");
        }
        this.chunkSize = chunkSize;

        if (threadsCount <= 0) {
            throw new IllegalArgumentException("threadsCount must be > 0");
        }
        ThreadPoolExecutor executorService;
        if (threadsCount > 1) {
            int executorThreadsCount = threadsCount - 1;
            executorService = new ThreadPoolExecutor(executorThreadsCount, executorThreadsCount,
                    0L, TimeUnit.MILLISECONDS, new SynchronousQueue<>());
            executorService.setRejectedExecutionHandler((r, executor) -> r.run());
        } else {
            executorService = null;
        }
        this.executorService = executorService;

    }

    public LargeFileSorter(Path sourceFilePath, Path targetFilePath) {
        this(sourceFilePath, targetFilePath, null, 30 * 1024 * 1024, 4);
    }

    public void sort() throws IOException, ExecutionException, InterruptedException {
        tempPath = Files.createTempDirectory(targetFilePath.getParent(), "tmp-");
        try {
            long start = System.currentTimeMillis();
            List<Path> sortedChunkFiles = splitToSortedChunks();
            System.out.println(String.format("%d chunks sorted in %d ms ...", sortedChunkFiles.size(), System.currentTimeMillis() - start));
            mergeSortedChunks(sortedChunkFiles);
            System.out.println(String.format("Chunks merged, done. Total time %d ms", System.currentTimeMillis() - start)); // TODO: использовать логгер вместо System.out
        } finally {
            if (executorService != null) {
                executorService.shutdown();
            }
            Files.delete(tempPath);
        }
    }

    /**
     * Делит исходный файл на несколько частей, сортируя каждую из них и сохраняя во временный файл.
     * В зависимости от конфигурации использует executorService с заданным числом потоков или выполняет всю работу в текущем потоке.
     * В случае, если при создании очередного чанка все потоки из пула в executorService заняты, выполняет сортировку в текущем потоке.
     */
    private List<Path> splitToSortedChunks() throws IOException, ExecutionException, InterruptedException {

        List<Future<Path>> futures = new ArrayList<>();
        try (BufferedReader reader = Files.newBufferedReader(sourceFilePath, CHARSET)) {
            Iterator<String> sourceIterator = reader.lines().iterator();
            while (sourceIterator.hasNext()) {
                List<String> chunk = readLinesChunk(sourceIterator);
                if (executorService != null) {
                    futures.add(executorService.submit(() -> sortAndSaveChunk(chunk)));
                } else {
                    futures.add(CompletableFuture.completedFuture(sortAndSaveChunk(chunk)));
                }
            }
        }

        List<Path> result = new ArrayList<>(futures.size());
        for (Future<Path> future : futures) {
            result.add(future.get());
        }
        return result;
    }

    /**
     * Мержит несколько отсортированных файлов в один.
     * Все переданные файлы открываются одновременно и обрабатываются в один проход.
     * Если файлов окажется очень много, стоит улучшить данный метод так,
     * чтобы поэтапно мержить файлы друг с другом в несколько проходов. Это позволит ограничить количество одновременно открытых файлов
     * и размер min-кучи, используемой для получения минимального в данный момент времени элемента.
     */
    private void mergeSortedChunks(List<Path> sortedChunks) throws IOException {

        int chunksCount = sortedChunks.size();
        Queue<ChunkIterator> queue = new PriorityQueue<>(chunksCount, (r1, r2) -> comparator.compare(r1.getCurrent(), r2.getCurrent()));
        for (Path readyChunkFile : sortedChunks) {
            ChunkIterator chunkReader = new ChunkIterator(readyChunkFile, Files.newBufferedReader(readyChunkFile, CHARSET));
            if (chunkReader.hasNext()) {
                chunkReader.next();
                queue.add(chunkReader);
            } else {
                chunkReader.closeAndDelete();
            }
        }

        try (BufferedWriter writer = Files.newBufferedWriter(targetFilePath, CHARSET)) {
            while (!queue.isEmpty()) {
                ChunkIterator smallest = queue.poll();
                writer.write(smallest.getCurrent());
                writer.newLine();
                if (smallest.hasNext()) {
                    smallest.next();
                    queue.add(smallest);
                } else {
                    smallest.closeAndDelete();
                }
            }
        }
    }

    private List<String> readLinesChunk(Iterator<String> iterator) {
        int currentChunkSize = 0; // очень приблизительный размер в памяти
        List<String> result = new ArrayList<>();
        while (iterator.hasNext() && currentChunkSize < chunkSize) {
            String line = iterator.next();
            result.add(line);
            currentChunkSize += line.length() * 2;
        }
        // в итоге фактический currentChunkSize окажется больше заданного chunkSize, но не критично
        return result;
    }

    private Path sortAndSaveChunk(List<String> lines) throws IOException {
        lines.sort(comparator);
        Path path = Files.createTempFile(tempPath, "chunk-", ".txt");
        try (BufferedWriter writer = Files.newBufferedWriter(path, CHARSET)) {
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
        }
        return path;
    }

    public static void main(String[] args) throws IOException, ExecutionException, InterruptedException {
        LargeFileSorter sorter = new LargeFileSorter(Paths.get("./file.txt"), Paths.get("./sorted.txt"));
        sorter.sort();
    }

}
